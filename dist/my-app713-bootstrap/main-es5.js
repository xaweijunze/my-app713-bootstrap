function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
  /***/
  "./$$_lazy_route_resource lazy recursive":
  /*!******************************************************!*\
    !*** ./$$_lazy_route_resource lazy namespace object ***!
    \******************************************************/

  /*! no static exports found */

  /***/
  function $$_lazy_route_resourceLazyRecursive(module, exports) {
    function webpackEmptyAsyncContext(req) {
      // Here Promise.resolve().then() is used instead of new Promise() to prevent
      // uncaught exception popping up in devtools
      return Promise.resolve().then(function () {
        var e = new Error("Cannot find module '" + req + "'");
        e.code = 'MODULE_NOT_FOUND';
        throw e;
      });
    }

    webpackEmptyAsyncContext.keys = function () {
      return [];
    };

    webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
    module.exports = webpackEmptyAsyncContext;
    webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
    /***/
  },

  /***/
  "./src/app/app-routing.module.ts":
  /*!***************************************!*\
    !*** ./src/app/app-routing.module.ts ***!
    \***************************************/

  /*! exports provided: AppRoutingModule */

  /***/
  function srcAppAppRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
      return AppRoutingModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

    var routes = [];

    var AppRoutingModule = function AppRoutingModule() {
      _classCallCheck(this, AppRoutingModule);
    };

    AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: AppRoutingModule
    });
    AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function AppRoutingModule_Factory(t) {
        return new (t || AppRoutingModule)();
      },
      imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, {
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/app.component.ts":
  /*!**********************************!*\
    !*** ./src/app/app.component.ts ***!
    \**********************************/

  /*! exports provided: AppComponent */

  /***/
  function srcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
      return AppComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _components_container_container_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./components/container/container.component */
    "./src/app/components/container/container.component.ts");

    var AppComponent = function AppComponent() {
      _classCallCheck(this, AppComponent);

      this.title = 'my-app713-bootstrap';
    };

    AppComponent.ɵfac = function AppComponent_Factory(t) {
      return new (t || AppComponent)();
    };

    AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: AppComponent,
      selectors: [["app-root"]],
      decls: 1,
      vars: 0,
      template: function AppComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-container");
        }
      },
      directives: [_components_container_container_component__WEBPACK_IMPORTED_MODULE_1__["ContainerComponent"]],
      styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQubGVzcyJ9 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-root',
          templateUrl: './app.component.html',
          styleUrls: ['./app.component.less']
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/app.module.ts":
  /*!*******************************!*\
    !*** ./src/app/app.module.ts ***!
    \*******************************/

  /*! exports provided: AppModule */

  /***/
  function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppModule", function () {
      return AppModule;
    });
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./app-routing.module */
    "./src/app/app-routing.module.ts");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./app.component */
    "./src/app/app.component.ts");
    /* harmony import */


    var _components_container_center_container_center_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./components/container-center/container-center.component */
    "./src/app/components/container-center/container-center.component.ts");
    /* harmony import */


    var _components_container_container_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./components/container/container.component */
    "./src/app/components/container/container.component.ts");
    /* harmony import */


    var _components_container_herder_container_herder_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./components/container-herder/container-herder.component */
    "./src/app/components/container-herder/container-herder.component.ts");
    /* harmony import */


    var _components_container_purple_container_purple_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./components/container-purple/container-purple.component */
    "./src/app/components/container-purple/container-purple.component.ts");
    /* harmony import */


    var _components_container_body_container_body_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./components/container-body/container-body.component */
    "./src/app/components/container-body/container-body.component.ts");
    /* harmony import */


    var _components_container_banner_container_banner_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./components/container-banner/container-banner.component */
    "./src/app/components/container-banner/container-banner.component.ts");

    var AppModule = function AppModule() {
      _classCallCheck(this, AppModule);
    };

    AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({
      type: AppModule,
      bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
    });
    AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({
      factory: function AppModule_Factory(t) {
        return new (t || AppModule)();
      },
      providers: [],
      imports: [[_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"]]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, {
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"], _components_container_center_container_center_component__WEBPACK_IMPORTED_MODULE_4__["ContainerCenterComponent"], _components_container_container_component__WEBPACK_IMPORTED_MODULE_5__["ContainerComponent"], _components_container_herder_container_herder_component__WEBPACK_IMPORTED_MODULE_6__["ContainerHerderComponent"], _components_container_purple_container_purple_component__WEBPACK_IMPORTED_MODULE_7__["ContainerPurpleComponent"], _components_container_body_container_body_component__WEBPACK_IMPORTED_MODULE_8__["ContainerBodyComponent"], _components_container_banner_container_banner_component__WEBPACK_IMPORTED_MODULE_9__["ContainerBannerComponent"]],
        imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
        args: [{
          declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"], _components_container_center_container_center_component__WEBPACK_IMPORTED_MODULE_4__["ContainerCenterComponent"], _components_container_container_component__WEBPACK_IMPORTED_MODULE_5__["ContainerComponent"], _components_container_herder_container_herder_component__WEBPACK_IMPORTED_MODULE_6__["ContainerHerderComponent"], _components_container_purple_container_purple_component__WEBPACK_IMPORTED_MODULE_7__["ContainerPurpleComponent"], _components_container_body_container_body_component__WEBPACK_IMPORTED_MODULE_8__["ContainerBodyComponent"], _components_container_banner_container_banner_component__WEBPACK_IMPORTED_MODULE_9__["ContainerBannerComponent"]],
          imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"]],
          providers: [],
          bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/container-banner/container-banner.component.ts":
  /*!***************************************************************************!*\
    !*** ./src/app/components/container-banner/container-banner.component.ts ***!
    \***************************************************************************/

  /*! exports provided: ContainerBannerComponent */

  /***/
  function srcAppComponentsContainerBannerContainerBannerComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContainerBannerComponent", function () {
      return ContainerBannerComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var ContainerBannerComponent =
    /*#__PURE__*/
    function () {
      function ContainerBannerComponent() {
        _classCallCheck(this, ContainerBannerComponent);
      }

      _createClass(ContainerBannerComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return ContainerBannerComponent;
    }();

    ContainerBannerComponent.ɵfac = function ContainerBannerComponent_Factory(t) {
      return new (t || ContainerBannerComponent)();
    };

    ContainerBannerComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: ContainerBannerComponent,
      selectors: [["app-container-banner"]],
      decls: 19,
      vars: 0,
      consts: [[1, "body"], [1, "text"], [1, "text_first", "container"], [1, "row"], [1, "col-lg-1", "col-md-1", "col-sm-1", "col-xs-3"], [1, "col-lg-offset-7", "col-lg-1", "col-md-offset-7", "col-md-1", "col-md-offset-7", "col-sm-4", "hidden-col-xs"], [1, "col-lg-12", "col-md-12", "col-sm-12", "col-xs-12", "text_second"]],
      template: function ContainerBannerComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "a", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "GitHub");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "a", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Twitter");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "a", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "\u5B9E\u4F8B");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "a", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "\u5173\u4E8E");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "p", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, " Designed and built with all the love in the world by @mdo and @fat. Maintained by the core team with the help of our contributors. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, " Code licensed MIT, docs CC BY 3.0. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      },
      styles: [".body[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n  background-color: #2b2631;\n  position: relative;\n}\n.text[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  font-size: 10px;\n}\n.text_first[_ngcontent-%COMP%] {\n  color: white;\n}\na[_ngcontent-%COMP%] {\n  color: white;\n}\n.text_second[_ngcontent-%COMP%] {\n  color: #9a979c;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy93ZWlqdW56ZS9hbmd1bGFyL215LWFwcDcxMy1ib290c3RyYXAvc3JjL2FwcC9jb21wb25lbnRzL2NvbnRhaW5lci1iYW5uZXIvY29udGFpbmVyLWJhbm5lci5jb21wb25lbnQubGVzcyIsInNyYy9hcHAvY29tcG9uZW50cy9jb250YWluZXItYmFubmVyL2NvbnRhaW5lci1iYW5uZXIuY29tcG9uZW50Lmxlc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7QUNDRjtBRENBO0VBQ0Usa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0VBQ0EsZUFBQTtBQ0NGO0FERUE7RUFDRSxZQUFBO0FDQUY7QURFQTtFQUNFLFlBQUE7QUNBRjtBREVBO0VBQ0UsY0FBQTtBQ0FGIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jb250YWluZXItYmFubmVyL2NvbnRhaW5lci1iYW5uZXIuY29tcG9uZW50Lmxlc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYm9keSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyYjI2MzE7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi50ZXh0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6NTAlO1xuICBsZWZ0OiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICBmb250LXNpemU6IDEwcHg7XG5cbn1cbi50ZXh0X2ZpcnN0IHtcbiAgY29sb3I6IHdoaXRlO1xufVxuYSB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi50ZXh0X3NlY29uZHtcbiAgY29sb3I6ICM5YTk3OWM7XG59XG4iLCIuYm9keSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyYjI2MzE7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi50ZXh0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDUwJTtcbiAgbGVmdDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgZm9udC1zaXplOiAxMHB4O1xufVxuLnRleHRfZmlyc3Qge1xuICBjb2xvcjogd2hpdGU7XG59XG5hIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuLnRleHRfc2Vjb25kIHtcbiAgY29sb3I6ICM5YTk3OWM7XG59XG4iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ContainerBannerComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-container-banner',
          templateUrl: './container-banner.component.html',
          styleUrls: ['./container-banner.component.less']
        }]
      }], function () {
        return [];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/container-body/container-body.component.ts":
  /*!***********************************************************************!*\
    !*** ./src/app/components/container-body/container-body.component.ts ***!
    \***********************************************************************/

  /*! exports provided: ContainerBodyComponent */

  /***/
  function srcAppComponentsContainerBodyContainerBodyComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContainerBodyComponent", function () {
      return ContainerBodyComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var ContainerBodyComponent =
    /*#__PURE__*/
    function () {
      function ContainerBodyComponent() {
        _classCallCheck(this, ContainerBodyComponent);
      }

      _createClass(ContainerBodyComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return ContainerBodyComponent;
    }();

    ContainerBodyComponent.ɵfac = function ContainerBodyComponent_Factory(t) {
      return new (t || ContainerBodyComponent)();
    };

    ContainerBodyComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: ContainerBodyComponent,
      selectors: [["app-container-body"]],
      decls: 2,
      vars: 0,
      template: function ContainerBodyComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "container-body works!");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      },
      styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29udGFpbmVyLWJvZHkvY29udGFpbmVyLWJvZHkuY29tcG9uZW50Lmxlc3MifQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ContainerBodyComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-container-body',
          templateUrl: './container-body.component.html',
          styleUrls: ['./container-body.component.less']
        }]
      }], function () {
        return [];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/container-center/container-center.component.ts":
  /*!***************************************************************************!*\
    !*** ./src/app/components/container-center/container-center.component.ts ***!
    \***************************************************************************/

  /*! exports provided: ContainerCenterComponent */

  /***/
  function srcAppComponentsContainerCenterContainerCenterComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContainerCenterComponent", function () {
      return ContainerCenterComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var ContainerCenterComponent =
    /*#__PURE__*/
    function () {
      function ContainerCenterComponent() {
        _classCallCheck(this, ContainerCenterComponent);
      }

      _createClass(ContainerCenterComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return ContainerCenterComponent;
    }();

    ContainerCenterComponent.ɵfac = function ContainerCenterComponent_Factory(t) {
      return new (t || ContainerCenterComponent)();
    };

    ContainerCenterComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: ContainerCenterComponent,
      selectors: [["app-container-center"]],
      decls: 2,
      vars: 0,
      template: function ContainerCenterComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "container-center works!");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      },
      styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29udGFpbmVyLWNlbnRlci9jb250YWluZXItY2VudGVyLmNvbXBvbmVudC5sZXNzIn0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ContainerCenterComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-container-center',
          templateUrl: './container-center.component.html',
          styleUrls: ['./container-center.component.less']
        }]
      }], function () {
        return [];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/container-herder/container-herder.component.ts":
  /*!***************************************************************************!*\
    !*** ./src/app/components/container-herder/container-herder.component.ts ***!
    \***************************************************************************/

  /*! exports provided: ContainerHerderComponent */

  /***/
  function srcAppComponentsContainerHerderContainerHerderComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContainerHerderComponent", function () {
      return ContainerHerderComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var ContainerHerderComponent =
    /*#__PURE__*/
    function () {
      function ContainerHerderComponent() {
        _classCallCheck(this, ContainerHerderComponent);
      }

      _createClass(ContainerHerderComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return ContainerHerderComponent;
    }();

    ContainerHerderComponent.ɵfac = function ContainerHerderComponent_Factory(t) {
      return new (t || ContainerHerderComponent)();
    };

    ContainerHerderComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: ContainerHerderComponent,
      selectors: [["app-container-herder"]],
      decls: 31,
      vars: 0,
      consts: [[1, "container", "container-header"], [1, "row"], [1, "head_title", "col-lg-2", "col-md-2", "col-sm-4", "col-xs-6"], [1, "col-lg-6", "col-md-6", "col-sm-8", "hidden-xs"], [1, "center"], ["href", ""], [1, "col-lg-4", "col-md-4", "hidden-sm", "hidden-xs"], [1, "right"], [1, "head_4", "hidden-lg", "hidden-md", "hidden-sm", "col-xs-6"]],
      template: function ContainerHerderComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "header", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "section", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "section", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, " bootstarp\u4E2D\u6587\u6587\u6863");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "section", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "\u5165\u95E8 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "\u5168\u5C40CSS\u6837\u5F0F ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "\u7EC4\u4EF6 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "JavaScript\u63D2\u4EF6 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "\u5B9A\u5236 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "\u7F51\u7AD9\u5B9E\u4F8B ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "section", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "v3 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "\u4F18\u9009\u7CBE\u9009 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "\u5B98\u65B9\u535A\u5BA2 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "\u8FD4\u56DEbootstrap\u4E2D\u6587\u7F51 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "section", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "a");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, " \u2261 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      },
      styles: ["header[_ngcontent-%COMP%] {\n  height: 60px;\n  color: #6b468a;\n}\n.head_title[_ngcontent-%COMP%] {\n  font-size: 18px;\n  white-space: nowrap;\n}\nheader[_ngcontent-%COMP%]    > section[_ngcontent-%COMP%] {\n  line-height: 60px;\n  height: 100%;\n  text-align: center;\n  font-weight: 700;\n}\n.center[_ngcontent-%COMP%], .right[_ngcontent-%COMP%] {\n  width: 80%;\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  white-space: nowrap;\n}\n.right[_ngcontent-%COMP%] {\n  width: 100%;\n}\na[_ngcontent-%COMP%] {\n  color: #6b468a;\n}\n.head_4[_ngcontent-%COMP%] {\n  font-weight: 200;\n  font-size: 30px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy93ZWlqdW56ZS9hbmd1bGFyL215LWFwcDcxMy1ib290c3RyYXAvc3JjL2FwcC9jb21wb25lbnRzL2NvbnRhaW5lci1oZXJkZXIvY29udGFpbmVyLWhlcmRlci5jb21wb25lbnQubGVzcyIsInNyYy9hcHAvY29tcG9uZW50cy9jb250YWluZXItaGVyZGVyL2NvbnRhaW5lci1oZXJkZXIuY29tcG9uZW50Lmxlc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFBO0VBQ0EsY0FBQTtBQ0NGO0FEQ0E7RUFDRSxlQUFBO0VBQ0EsbUJBQUE7QUNDRjtBREVBO0VBQ0UsaUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ0FGO0FERUE7O0VBQ0UsVUFBQTtFQUNBLGFBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUNDRjtBRENBO0VBQ0UsV0FBQTtBQ0NGO0FERUE7RUFDRSxjQUFBO0FDQUY7QURFQTtFQUNFLGdCQUFBO0VBQ0EsZUFBQTtBQ0FGIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jb250YWluZXItaGVyZGVyL2NvbnRhaW5lci1oZXJkZXIuY29tcG9uZW50Lmxlc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoZWFkZXIge1xuICBoZWlnaHQ6IDYwcHg7XG4gIGNvbG9yOiAjNmI0NjhhO1xufVxuLmhlYWRfdGl0bGUge1xuICBmb250LXNpemU6IDE4cHg7XG4gIHdoaXRlLXNwYWNlIDogIG5vd3JhcDtcblxufVxuaGVhZGVyPnNlY3Rpb24ge1xuICBsaW5lLWhlaWdodDogNjBweDtcbiAgaGVpZ2h0OiAxMDAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG4uY2VudGVyLC5yaWdodHtcbiAgd2lkdGg6IDgwJTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB3aGl0ZS1zcGFjZSA6ICBub3dyYXA7XG59XG4ucmlnaHQge1xuICB3aWR0aDogMTAwJTtcblxufVxuYSB7XG4gIGNvbG9yOiAjNmI0NjhhO1xufVxuLmhlYWRfNCB7XG4gIGZvbnQtd2VpZ2h0OiAyMDA7XG4gIGZvbnQtc2l6ZTogMzBweDtcbn1cbiIsImhlYWRlciB7XG4gIGhlaWdodDogNjBweDtcbiAgY29sb3I6ICM2YjQ2OGE7XG59XG4uaGVhZF90aXRsZSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbn1cbmhlYWRlciA+IHNlY3Rpb24ge1xuICBsaW5lLWhlaWdodDogNjBweDtcbiAgaGVpZ2h0OiAxMDAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG4uY2VudGVyLFxuLnJpZ2h0IHtcbiAgd2lkdGg6IDgwJTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xufVxuLnJpZ2h0IHtcbiAgd2lkdGg6IDEwMCU7XG59XG5hIHtcbiAgY29sb3I6ICM2YjQ2OGE7XG59XG4uaGVhZF80IHtcbiAgZm9udC13ZWlnaHQ6IDIwMDtcbiAgZm9udC1zaXplOiAzMHB4O1xufVxuIl19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ContainerHerderComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-container-herder',
          templateUrl: './container-herder.component.html',
          styleUrls: ['./container-herder.component.less']
        }]
      }], function () {
        return [];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/container-purple/container-purple.component.ts":
  /*!***************************************************************************!*\
    !*** ./src/app/components/container-purple/container-purple.component.ts ***!
    \***************************************************************************/

  /*! exports provided: ContainerPurpleComponent */

  /***/
  function srcAppComponentsContainerPurpleContainerPurpleComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContainerPurpleComponent", function () {
      return ContainerPurpleComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var ContainerPurpleComponent =
    /*#__PURE__*/
    function () {
      function ContainerPurpleComponent() {
        _classCallCheck(this, ContainerPurpleComponent);
      }

      _createClass(ContainerPurpleComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return ContainerPurpleComponent;
    }();

    ContainerPurpleComponent.ɵfac = function ContainerPurpleComponent_Factory(t) {
      return new (t || ContainerPurpleComponent)();
    };

    ContainerPurpleComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: ContainerPurpleComponent,
      selectors: [["app-container-purple"]],
      decls: 15,
      vars: 0,
      consts: [[1, "body"], [1, "text"], [1, "head_title"], [1, "head_text"], ["type", "button", 1, "btn-block", "btn-default"], [1, "bottom-text"]],
      template: function ContainerPurpleComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "B");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, " Bootstrap \u662F\u6700\u53D7\u6B22\u8FCE\u7684 HTML\u3001CSS \u548C JS \u6846\u67B6\uFF0C\u7528\u4E8E\u5F00\u53D1\u54CD\u5E94\u5F0F\u5E03");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, " \u5C40\u3001\u79FB\u52A8\u8BBE\u5907\u4F18\u5148\u7684 WEB \u9879\u76EE\u3002 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "\u4E0B\u8F7Dbootstrap");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "\u5F53\u524D\u7248\u672C\uFF1A v3.4.1 | \u6587\u6863\u66F4\u65B0\u4E8E\uFF1A2021-06-10");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      },
      styles: [".body[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n  \n  \n  \n  background: linear-gradient(#5c3380, #764a9d);\n  position: relative;\n  color: white;\n}\nh1[_ngcontent-%COMP%], p[_ngcontent-%COMP%] {\n  padding: 0;\n  margin: 0;\n}\n.head_title[_ngcontent-%COMP%] {\n  font-size: 100px;\n  height: 120px;\n  width: 120px;\n  line-height: 120px;\n  text-align: center;\n  white-space: nowrap;\n  border: white solid 1px;\n  border-radius: 20px;\n  margin: auto;\n}\n.head_text[_ngcontent-%COMP%] {\n  color: white;\n  font-weight: 200;\n  line-height: 1.4;\n  font-size: 24px;\n  white-space: nowrap;\n  text-align: center;\n}\n.text[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n.bottom-text[_ngcontent-%COMP%] {\n  text-align: center;\n  color: #9d7dbd;\n}\n.btn-block[_ngcontent-%COMP%] {\n  width: 40%;\n  margin: 0 auto;\n  height: 50px;\n  border-radius: 20px;\n  color: #5c3380;\n  font-size: 26px;\n  font-weight: 700;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy93ZWlqdW56ZS9hbmd1bGFyL215LWFwcDcxMy1ib290c3RyYXAvc3JjL2FwcC9jb21wb25lbnRzL2NvbnRhaW5lci1wdXJwbGUvY29udGFpbmVyLXB1cnBsZS5jb21wb25lbnQubGVzcyIsInNyYy9hcHAvY29tcG9uZW50cy9jb250YWluZXItcHVycGxlL2NvbnRhaW5lci1wdXJwbGUuY29tcG9uZW50Lmxlc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQ0VBLG9CQUFvQjtFQUVwQixzQkFBc0I7RUFFdEIscUJBQXFCO0VEQ3JCLDZDQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDQ0Y7QURDQTs7RUFDRSxVQUFBO0VBQ0EsU0FBQTtBQ0VGO0FEQUE7RUFDRSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUNFRjtBRENBO0VBQ0UsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQ0NGO0FERUE7RUFDRSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsZ0NBQUE7QUNBRjtBREVBO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0FDQUY7QURHQTtFQUNFLFVBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0RGIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jb250YWluZXItcHVycGxlL2NvbnRhaW5lci1wdXJwbGUuY29tcG9uZW50Lmxlc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYm9keSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KCM1YzMzODAsICM3NjRhOWQpO1xuICAvKiBTYWZhcmkgNS4xIC0gNi4wKi9cbiAgYmFja2dyb3VuZDogLW8tbGluZWFyLWdyYWRpZW50KCM1YzMzODAsICM3NjRhOWQpO1xuICAvKiBPcGVyYSAxMS4xIC0gMTIuMCAqL1xuICBiYWNrZ3JvdW5kOiAtbW96LWxpbmVhci1ncmFkaWVudCgjNWMzMzgwLCAjNzY0YTlkKTtcbiAgLyogRmlyZWZveCAzLjYgLSAxNSAqL1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoIzVjMzM4MCwgIzc2NGE5ZCk7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgY29sb3I6IHdoaXRlO1xufVxuaDEscCB7XG4gIHBhZGRpbmc6IDA7XG4gIG1hcmdpbjogMDtcbn1cbi5oZWFkX3RpdGxlIHtcbiAgZm9udC1zaXplOiAxMDBweDtcbiAgaGVpZ2h0OiAxMjBweDtcbiAgd2lkdGg6IDEyMHB4O1xuICBsaW5lLWhlaWdodDogMTIwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgd2hpdGUtc3BhY2UgOiAgbm93cmFwO1xuICBib3JkZXI6IHdoaXRlIHNvbGlkIDFweDtcbiAgYm9yZGVyLXJhZGl1czogIDIwcHg7XG4gIG1hcmdpbjogYXV0bztcblxufVxuLmhlYWRfdGV4dHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXdlaWdodDogMjAwO1xuICBsaW5lLWhlaWdodDogMS40O1xuICBmb250LXNpemU6IDI0cHg7XG4gIHdoaXRlLXNwYWNlIDogIG5vd3JhcDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG59XG4udGV4dCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA1MCU7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG59XG4uYm90dG9tLXRleHR7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICM5ZDdkYmQ7XG5cbn1cbi5idG4tYmxvY2sge1xuICB3aWR0aDogNDAlO1xuICBtYXJnaW46IDAgYXV0bztcbiAgaGVpZ2h0OiA1MHB4O1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICBjb2xvcjogIzVjMzM4MDtcbiAgZm9udC1zaXplOiAyNnB4O1xuICBmb250LXdlaWdodDogNzAwO1xufVxuXG4iLCIuYm9keSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KCM1YzMzODAsICM3NjRhOWQpO1xuICAvKiBTYWZhcmkgNS4xIC0gNi4wKi9cbiAgYmFja2dyb3VuZDogLW8tbGluZWFyLWdyYWRpZW50KCM1YzMzODAsICM3NjRhOWQpO1xuICAvKiBPcGVyYSAxMS4xIC0gMTIuMCAqL1xuICBiYWNrZ3JvdW5kOiAtbW96LWxpbmVhci1ncmFkaWVudCgjNWMzMzgwLCAjNzY0YTlkKTtcbiAgLyogRmlyZWZveCAzLjYgLSAxNSAqL1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoIzVjMzM4MCwgIzc2NGE5ZCk7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgY29sb3I6IHdoaXRlO1xufVxuaDEsXG5wIHtcbiAgcGFkZGluZzogMDtcbiAgbWFyZ2luOiAwO1xufVxuLmhlYWRfdGl0bGUge1xuICBmb250LXNpemU6IDEwMHB4O1xuICBoZWlnaHQ6IDEyMHB4O1xuICB3aWR0aDogMTIwcHg7XG4gIGxpbmUtaGVpZ2h0OiAxMjBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBib3JkZXI6IHdoaXRlIHNvbGlkIDFweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgbWFyZ2luOiBhdXRvO1xufVxuLmhlYWRfdGV4dCB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC13ZWlnaHQ6IDIwMDtcbiAgbGluZS1oZWlnaHQ6IDEuNDtcbiAgZm9udC1zaXplOiAyNHB4O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4udGV4dCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA1MCU7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG59XG4uYm90dG9tLXRleHQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiAjOWQ3ZGJkO1xufVxuLmJ0bi1ibG9jayB7XG4gIHdpZHRoOiA0MCU7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBoZWlnaHQ6IDUwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGNvbG9yOiAjNWMzMzgwO1xuICBmb250LXNpemU6IDI2cHg7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG4iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ContainerPurpleComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-container-purple',
          templateUrl: './container-purple.component.html',
          styleUrls: ['./container-purple.component.less']
        }]
      }], function () {
        return [];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/container/container.component.ts":
  /*!*************************************************************!*\
    !*** ./src/app/components/container/container.component.ts ***!
    \*************************************************************/

  /*! exports provided: ContainerComponent */

  /***/
  function srcAppComponentsContainerContainerComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContainerComponent", function () {
      return ContainerComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _container_herder_container_herder_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../container-herder/container-herder.component */
    "./src/app/components/container-herder/container-herder.component.ts");
    /* harmony import */


    var _container_purple_container_purple_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../container-purple/container-purple.component */
    "./src/app/components/container-purple/container-purple.component.ts");
    /* harmony import */


    var _container_body_container_body_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../container-body/container-body.component */
    "./src/app/components/container-body/container-body.component.ts");
    /* harmony import */


    var _container_banner_container_banner_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../container-banner/container-banner.component */
    "./src/app/components/container-banner/container-banner.component.ts");

    var ContainerComponent =
    /*#__PURE__*/
    function () {
      function ContainerComponent() {
        _classCallCheck(this, ContainerComponent);
      }

      _createClass(ContainerComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return ContainerComponent;
    }();

    ContainerComponent.ɵfac = function ContainerComponent_Factory(t) {
      return new (t || ContainerComponent)();
    };

    ContainerComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: ContainerComponent,
      selectors: [["app-container"]],
      decls: 7,
      vars: 0,
      consts: [[1, "container-purple"], [1, "container-body"], [1, "container-banner"]],
      template: function ContainerComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-container-herder");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "section", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "app-container-purple");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "section", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "app-container-body");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "section", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "app-container-banner");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      },
      directives: [_container_herder_container_herder_component__WEBPACK_IMPORTED_MODULE_1__["ContainerHerderComponent"], _container_purple_container_purple_component__WEBPACK_IMPORTED_MODULE_2__["ContainerPurpleComponent"], _container_body_container_body_component__WEBPACK_IMPORTED_MODULE_3__["ContainerBodyComponent"], _container_banner_container_banner_component__WEBPACK_IMPORTED_MODULE_4__["ContainerBannerComponent"]],
      styles: [".border[_ngcontent-%COMP%] {\n  border: black solid 1px;\n}\n.container-purple[_ngcontent-%COMP%] {\n  height: 550px;\n  width: 100%;\n}\n.container-body[_ngcontent-%COMP%] {\n  height: 600px;\n  width: 100%;\n}\n.container-banner[_ngcontent-%COMP%] {\n  height: 160px;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy93ZWlqdW56ZS9hbmd1bGFyL215LWFwcDcxMy1ib290c3RyYXAvc3JjL2FwcC9jb21wb25lbnRzL2NvbnRhaW5lci9jb250YWluZXIuY29tcG9uZW50Lmxlc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvY29udGFpbmVyL2NvbnRhaW5lci5jb21wb25lbnQubGVzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHVCQUFBO0FDQ0Y7QURFQTtFQUNFLGFBQUE7RUFDQSxXQUFBO0FDQUY7QURHQTtFQUNFLGFBQUE7RUFDQSxXQUFBO0FDREY7QURHQTtFQUNFLGFBQUE7RUFDQSxXQUFBO0FDREYiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2NvbnRhaW5lci9jb250YWluZXIuY29tcG9uZW50Lmxlc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYm9yZGVyIHtcbiAgYm9yZGVyOiBibGFjayBzb2xpZCAxcHg7XG59XG5cbi5jb250YWluZXItcHVycGxlIHtcbiAgaGVpZ2h0OiA1NTBweDtcbiAgd2lkdGg6IDEwMCU7XG5cbn1cbi5jb250YWluZXItYm9keSB7XG4gIGhlaWdodDogNjAwcHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmNvbnRhaW5lci1iYW5uZXIge1xuICBoZWlnaHQ6IDE2MHB4O1xuICB3aWR0aDogMTAwJTtcbn1cblxuXG4iLCIuYm9yZGVyIHtcbiAgYm9yZGVyOiBibGFjayBzb2xpZCAxcHg7XG59XG4uY29udGFpbmVyLXB1cnBsZSB7XG4gIGhlaWdodDogNTUwcHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmNvbnRhaW5lci1ib2R5IHtcbiAgaGVpZ2h0OiA2MDBweDtcbiAgd2lkdGg6IDEwMCU7XG59XG4uY29udGFpbmVyLWJhbm5lciB7XG4gIGhlaWdodDogMTYwcHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuIl19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ContainerComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-container',
          templateUrl: './container.component.html',
          styleUrls: ['./container.component.less']
        }]
      }], function () {
        return [];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/environments/environment.ts":
  /*!*****************************************!*\
    !*** ./src/environments/environment.ts ***!
    \*****************************************/

  /*! exports provided: environment */

  /***/
  function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "environment", function () {
      return environment;
    }); // This file can be replaced during build by using the `fileReplacements` array.
    // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
    // The list of file replacements can be found in `angular.json`.


    var environment = {
      production: false
    };
    /*
     * For easier debugging in development mode, you can import the following file
     * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
     *
     * This import should be commented out in production mode because it will have a negative impact
     * on performance if an error is thrown.
     */
    // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

    /***/
  },

  /***/
  "./src/main.ts":
  /*!*********************!*\
    !*** ./src/main.ts ***!
    \*********************/

  /*! no exports provided */

  /***/
  function srcMainTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./app/app.module */
    "./src/app/app.module.ts");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");

    if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
      Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
    }

    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])["catch"](function (err) {
      return console.error(err);
    });
    /***/

  },

  /***/
  0:
  /*!***************************!*\
    !*** multi ./src/main.ts ***!
    \***************************/

  /*! no static exports found */

  /***/
  function _(module, exports, __webpack_require__) {
    module.exports = __webpack_require__(
    /*! /Users/weijunze/angular/my-app713-bootstrap/src/main.ts */
    "./src/main.ts");
    /***/
  }
}, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es5.js.map