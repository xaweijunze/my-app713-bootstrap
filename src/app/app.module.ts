import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContainerCenterComponent } from './components/container-center/container-center.component';
import { ContainerComponent } from './components/container/container.component';
import { ContainerHerderComponent } from './components/container-herder/container-herder.component';
import { ContainerPurpleComponent } from './components/container-purple/container-purple.component';
import { ContainerBodyComponent } from './components/container-body/container-body.component';
import { ContainerBannerComponent } from './components/container-banner/container-banner.component';

@NgModule({
  declarations: [
    AppComponent,
    ContainerCenterComponent,
    ContainerComponent,
    ContainerHerderComponent,
    ContainerPurpleComponent,
    ContainerBodyComponent,
    ContainerBannerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
