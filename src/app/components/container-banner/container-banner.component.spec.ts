import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerBannerComponent } from './container-banner.component';

describe('ContainerBannerComponent', () => {
  let component: ContainerBannerComponent;
  let fixture: ComponentFixture<ContainerBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
