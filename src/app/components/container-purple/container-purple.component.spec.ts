import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerPurpleComponent } from './container-purple.component';

describe('ContainerPurpleComponent', () => {
  let component: ContainerPurpleComponent;
  let fixture: ComponentFixture<ContainerPurpleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerPurpleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerPurpleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
