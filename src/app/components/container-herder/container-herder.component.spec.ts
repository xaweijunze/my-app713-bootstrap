import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerHerderComponent } from './container-herder.component';

describe('ContainerHerderComponent', () => {
  let component: ContainerHerderComponent;
  let fixture: ComponentFixture<ContainerHerderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerHerderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerHerderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
